let collection = [];

// Write the queue functions below.

function print() {
    return collection;
}

function enqueue(element) {
   collection[collection.length] = element
   return  collection;
}

function dequeue() {
    for (i = 0; i < collection.length - 1; i++){
        // collection[i - 1] = collection[i];

        collection[i] = collection[i+1]
     }   
        collection.length--
        return collection;
}

function front() {
    return collection[0]
}

function size() {
    return collection.length;
}

function isEmpty() {
    if (collection.length < 1) {
        return true
    } else {
        return false
    }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};